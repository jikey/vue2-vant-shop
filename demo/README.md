# demo
Vue2 + Vant Demo 集合 

## Project setup
```
npm install
npm run serve
npm run build
```

### Preview
|  序号   | 链接  |  备注  |
|  ----  | ----  | ----  |
| 1  | [CSS 图片模糊](https://jikeytang.bitbucket.io/demo/#/case/case-01) |  |
| 2  | [新手引导](https://jikeytang.bitbucket.io/demo/#/case/case-02) |  |
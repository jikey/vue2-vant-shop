import { title, name, version, description, author, } from '../../package'

export default {
  title,
  name,
  version,
  author,
  description,
  // 侧边栏默认折叠状态
  sideBar: {
    sideBarCollapse: false,
  },
  page: {
    opened: [
      {
        name: 'index',
        meta: {
          title: '首页',
          requiresAuth: false,
        },
      }
    ],
  },
  // 注册的主题
  user: {
    info: {
      name: 'Jiname',
    },
  },
}

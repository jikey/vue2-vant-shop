export default [
  {
    path: ``,
    name: 'home',
    component: () => import('@/views/index'),
    meta: { title: '公告板', icon: 'home', affix: true, },
  }
]

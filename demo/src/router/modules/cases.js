const pre = '/case/'

export default [
  {
    path: `${pre}index`,
    name: 'cases',
    component: () => import('@/views/case'),
    meta: { icon: 'user-list', title: '案例列表', },
  },
  {
    path: `${pre}case-01`,
    name: 'cases',
    component: () => import('@/views/case/case-01'),
    meta: { icon: 'user-list', title: '图片模糊', },
  },
  {
    path: `${pre}case-02`,
    name: 'cases',
    component: () => import('@/views/case/case-02'),
    meta: { icon: 'user-list', title: '新手引导', },
  }
]

import Layout from '@/layout'
import home from './modules/home'
import cases from './modules/cases'

export default [
  {
    path: '/404',
    component: () => import('@/views/other/404'),
    hidden: true,
  },
  {
    path: '/',
    component: Layout,
    title: '首页',
    icon: 'home',
    redirect: { name: 'dashboard', },
    children: home,
  },
  {
    path: '/case',
    component: Layout,
    icon: 'user',
    title: '案例管理',
    children: cases,
  }
]
